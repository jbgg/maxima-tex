
prog=tm

installdir=~/.local/bin

.PHONY: default install example test

all: default

default:
	@echo "Use make install|example|test"

install: $(prog)
	install $(prog) $(installdir)

example:
	@make -C example

test:
	@make -C test

clean:
	@make -C example clean
	@make -C test clean
