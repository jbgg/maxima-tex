%!TeX root=main.tex
% This is a comment in tm script
This text will be printed literally in the document.
The variable $a$ has value $2$.
% Variabe a will be defined
%c a:2$
% A complex operation with variable a of maxima
%c a^2$
% The %d command will show the previous output
% in maxima commands. In this case will be the value
% of a^2.
Then, the value of $a^2$ is
%d $ $.
