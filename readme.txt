maxima-tex is the name of the repo.

In this repo there are a simple script (tm) and
a source code of TeX macros (maxima.tex).

`maxima.tex` defines several macros for display maxima
code and output as an interactive session of maxima.

The script `tm` process commands calling maxima and
printing the output.
