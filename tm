#!/usr/bin/env sh

get_command(){
	printf "%s\n" "$1" | sed -n  's/^[[:blank:]]*%\([^[:blank:]]*\).*$/\1/p'
}

get_arg1(){
	printf "%s\n" "$1" | sed -n 's/^[[:blank:]]*%[^[:blank:]]*[[:blank:]]*\([^[:blank:]]*\).*$/\1/p'
}

get_arg2(){
	printf "%s\n" "$1" | sed -n 's/^[[:blank:]]*%[^[:blank:]]*[[:blank:]]*[^[:blank:]]*[[:blank:]]*\([^[:blank:]]*\).*$/\1/p'
}

get_args(){
	printf "%s\n" "$1" | sed -n 's/^[[:blank:]]*%[^[:blank:]]*[[:blank:]]*\(.*\)$/\1/p'
}

# list of commands
# %% # comment
# %e # echo
# %c # append a maxima command

# %r [filename] # output a file
# %r*d* [filename] # output a file and delete it

# %d # display previous command
# %dv # display previous command showing as vector, changing [ and ] by ( and )
# %do # display previous command and display in maximao environment

# %cd, %cdv, %cdo # command c and d, dv or do

# %ei maximai environment
# %el maximal environment

# global variables
g_cmd=""

# gloabl variables used for m (matrix) command
m_name=""
m_firstq=""
m_rows=""

settexenvironment='set_tex_environment_default("","");'
g_cmds="$g_cmds $settexenvironment"

outputfile=`mktemp`

while read -r line; do
	# print line
	printf "%s\n" "${line}" >> "$outputfile"
	# read command
	g_cmd=`get_command "$line"`
	case $g_cmd in

		e)
			test="`get_args "$line"`"
			printf "%s\n" "$test" >> "$outputfile"
			;;

		c)
			g_cmds="$g_cmds `get_args "$line"`"
			;;

		d|dv|do|cd|cdv|cdo)
			case $g_cmd in
				c*)
				g_cmds="$g_cmds `get_args "$line"`"
				;;
			esac
			d_env_ini=
			d_env_end=
			case $g_cmd in
				d|dv)
					d_env_ini="`get_arg1 "$line"`"
					d_env_end="`get_arg2 "$line"`"
					;;
				do|cdo)
					d_env_ini="\begin{maximao}"
					d_env_end="\end{maximao}"
					;;
			esac
			# print environment begin
			if [ -n "$d_env_ini" ]; then
				printf "%s\n" "$d_env_ini" >> "$outputfile"
			fi
			tmpfile=`mktemp`
			# get output in tmpfile
			g_cmds="$g_cmds tex(%, \"$tmpfile\");"
			# adding %r*d* command
			printf '%s%s %s\n' '%r' "$g_cmd" "$tmpfile" >> "$outputfile"
			# print environment end
			if [ -n "$d_env_end" ]; then
				printf "%s\n" "$d_env_end" >> "$outputfile"
			fi
			;;

		e*)
			l_cmd="`get_args "$line"`"
			g_cmds="$g_cmds $l_cmd"
			case $g_cmd in
				ei)
					printf "%s\n" "\begin{maximai}" >> "$outputfile"
					printf "%s\n" "$l_cmd" >> "$outputfile"
					printf "%s\n" "\end{maximai}" >> "$outputfile"
					;;
				el)
					printf "%s\n" "\begin{maximal}" >> "$outputfile"
					printf "%s\n" "$l_cmd" >> "$outputfile"
					printf "%s\n" "\end{maximal}" >> "$outputfile"
					;;
			esac
			;;

	esac
done

maxima --very-quiet \
--batch-string="$g_cmds;" < /dev/null > /dev/null

while read -r line; do
	g_cmd=`get_command "$line"`
	case $g_cmd in

		r|rd|rdv|rdo|rcd|rcdv|rcdo)
			tmpfile=`get_args "$line"`
			case $g_cmd in
				*v*)
				# [...] changed to (...)
				sed -i -e 's,\[,(,g' -e 's,\],),g' "$tmpfile"
			esac
			if [ "$g_cmd" = "r" ]; then
				printf "%s\n" "${line}"
			fi
			cat "$tmpfile"
			case $g_cmd in
				*d*)
				rm -f "$tmpfile"
			esac
		;;

		*)
			printf "%s\n" "${line}"
		;;
	esac
done < "$outputfile"
rm -f "$outputfile"
